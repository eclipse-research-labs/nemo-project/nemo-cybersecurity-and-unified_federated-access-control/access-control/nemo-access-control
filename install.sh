#!/bin/bash

TICK='\U2705'
X='\U274C'
GREEN='\e[92m'
YELLOW='\e[93m'
RED='\e[91m'
DEF='\e[0m'
ERROR="Something went wrong!\n"
DONE="Done!\n"
ERROR_MSG="${X}${RED} ${ERROR_MSG}${DEF}"
DONE_MSG="${TICK}${GREEN} ${DONE}${DEF}"

NAMESPACE=$(jq -r '.namespace' config/access-control.json)
PASSWORD=$(jq -r '.password' config/access-control.json)
ADMIN_NODE_PORT=$(jq -r '.admin_node_port' config/access-control.json)
ADMIN_GUI_URL=$(jq -r '.admin_gui_api_url' config/access-control.json)

PASSWORD=$(echo -e "${PASSWORD}" | base64)

kubecmd=${1:-"kubectl"}
helmcmd=${2:-"helm"}

# Give read-write permissions for JSON configs
chmod -R +rw config/

# Install jq library
if [[ ! $(which jq) || ! $(which yq) ]]; then
    echo -e "${YELLOW}Installing jq & yq..."
    sudo apt update && sudo apt install -y jq && sudo snap install yq
fi

echo -e "This is the Access Control module of the NEMO project.\nInstallation will begin shortly..\U23F3\n"
echo -e "\U2755${YELLOW}Please make sure you have installed on your machine the prerequisites before installation!${DEF}\n"

password="$PASSWORD" yq -i '.data."postgres-password" = env(password)' manifests/secret.yaml
admin_node_port="$ADMIN_NODE_PORT" yq -i '.admin.http.nodePort = env(admin_node_port)' manifests/kong.yaml
admin_gui_url="$ADMIN_GUI_URL" yq -i '.env.admin_gui_api_url = env(admin_gui_url)' manifests/kong.yaml

${kubecmd} apply -f manifests/secret.yaml -n $NAMESPACE

echo -e "- Adding Kong Helm chart...\n"
${helmcmd} repo add kong https://charts.konghq.com -n $NAMESPACE
if test $? -eq 0
then
echo -e "\U2705\e[92m Done!\n\e[0m"
echo -e "- Updating Helm...\n"
${helmcmd} repo update -n $NAMESPACE
if test $? -eq 0
then
    echo -e "\n${DONE_MSG}"
else
    echo -e "\n${ERROR_MSG}"
    exit 1
fi
else
echo -e "\n${ERROR_MSG}"
exit 1
fi


echo -e "- Installing Kong...\n"
${helmcmd} -n $NAMESPACE upgrade --install nemo-kong kong/kong --version 2.8.0 -f manifests/kong.yaml --set proxy.type=NodePort --set ingressController.watchNamespaces={$NAMESPACE} --set ingressController.ingressClass=$NAMESPACE
if test $? -eq 0
then
    echo -e "\n${DONE_MSG}"
else
    echo -e "\n${ERROR_MSG}"
    exit 1
fi

echo -e "\e[92mInstallation complete! Ready to use the Access Control module!\n\e[0m"
export NODE_PORT=$(${kubecmd} get --namespace $NAMESPACE -o jsonpath="{.spec.ports[0].nodePort}" services nemo-kong-kong-manager)
export NODE_IP=$(${kubecmd} get nodes --namespace $NAMESPACE -o jsonpath="{.items[0].status.addresses[0].address}")
PAGE=$(echo -e http://$NODE_IP:$NODE_PORT)
echo -e "-> Visit the module here: $PAGE"

exit 0
