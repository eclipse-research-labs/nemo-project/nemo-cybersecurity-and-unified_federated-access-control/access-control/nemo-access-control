#!/bin/bash

TICK='\U2705'
X='\U274C'
GREEN='\e[92m'
YELLOW='\e[93m'
RED='\e[91m'
DEF='\e[0m'
ERROR="Something went wrong!\n"
DONE="Done!\n"
ERROR_MSG="${X}${RED} ${ERROR_MSG}${DEF}"
DONE_MSG="${TICK}${GREEN} ${DONE}${DEF}"

NAMESPACE=$(jq -r '.namespace' config/access-control.json)

echo -e "Removing the Access control module...\U1F6E0\n\n"

echo -e "- Removing Kong...\n"
helm uninstall nemo-kong -n $NAMESPACE
if test $? -eq 0
then
    echo -e "\n${DONE_MSG}"
else
    echo -e "\n${ERROR_MSG}"
    exit 1
fi

kubectl delete pvc data-nemo-kong-postgresql-0 -n $NAMESPACE

echo -e "Access Control module uninstalled. \U1F5D1\nTo install again run: ./install.sh"

exit 0 