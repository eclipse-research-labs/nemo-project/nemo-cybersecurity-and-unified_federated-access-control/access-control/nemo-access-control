package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/Kong/go-pdk"
	"github.com/Kong/go-pdk/server"
	"github.com/Nerzal/gocloak/v12"
)

func main() {
	server.StartServer(New, Version, Priority)
}

var Version = "0.2"
var Priority = 1004

type Config struct {
	KeycloakURL  string
	ClientID     string
	ClientSecret string
	Realm        string
}

func New() interface{} {
	return &Config{}
}

func (conf Config) Access(kong *pdk.PDK) {
	LOG_FILE := "/tmp/keycloak_log"
	logfile, _ := os.OpenFile(LOG_FILE, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	var checker bool
	checker = false
	var status int
	var message string
	defer logfile.Close()
	log.SetOutput(logfile)
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred:", err)
			message := []byte("Invalid")
			x := make(map[string][]string)
			x["Content-Type"] = append(x["Content-Type"], "application/json")
			// kong.Response.Exit(int(400), message, x)
			kong.Response.Exit(status, message, x)
		}
	}()
	token, err := kong.Request.GetHeader("token")
	if err != nil {
		message = "Failed getting the token header"
		log.Print(err)
		checker = true
		status = 400
	}

	if token == "" && !checker {
		message = "Token not provided"
		log.Println(message)
		checker = true
		status = 400
	}
	if !checker {
		log.Printf("Trying to authenticate using token: %s", token)
		ctx := context.Background()
		//keycloak client url should be configurable
		client := gocloak.NewClient(conf.KeycloakURL)

		if !checker {
			if conf.ClientSecret != "public" {
				rptResult, err := client.RetrospectToken(ctx, token, conf.ClientID, conf.ClientSecret, conf.Realm)
				if err != nil {
					message = "Token inspection failed"
					log.Println("Inspection faled: " + fmt.Sprint(err))
					checker = true
					status = 403
				}
				if !(*rptResult.Active) && !checker {
					message = "Token is not active."
					log.Println("Error token is not active")
					log.Print(*rptResult.Active)
					checker = true
					status = 401
				}
			} else {
				jwt, _, err := client.DecodeAccessToken(ctx, token, conf.Realm)
				if err != nil {
					message = fmt.Sprint(err)
					log.Println(err)
					status = 401
					checker = true
				}
				//log.Println(jwt.Valid)
				//log.Println(claims.Valid())
				if !jwt.Valid && !checker {
					checker = true
					message = "Token is not valid"
					status = 401
					log.Println("Token is not valid")
				}
			}
		}
	}
	if checker {
		log.Println("exiting..")
		// kong.Response.Exit(status, message, nil)
		x := make(map[string][]string)
		x["Content-Type"] = append(x["Content-Type"], "application/json")
		kong.Response.Exit(status, []byte(message), x)
	} else {
		log.Println("User is authorized")
		kong.Response.SetHeader("Authorization", "User authorized")
	}
}
