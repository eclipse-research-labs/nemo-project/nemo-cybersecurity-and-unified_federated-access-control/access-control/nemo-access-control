package main

import (
	"io"
	"log"
	"net/http"
	"os"

	"github.com/Kong/go-pdk"
	"github.com/Kong/go-pdk/server"
)

func main() {
	server.StartServer(New, Version, Priority)
}

var Version = "0.2"
var Priority = 1

type Config struct {
	IAAProxy string
}

func New() interface{} {
	return &Config{}
}

func (conf Config) Access(kong *pdk.PDK) {

	LOG_FILE := "/tmp/ssi_log"
	logfile, err := os.OpenFile(LOG_FILE, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer logfile.Close()
	log.SetOutput(logfile)
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	jwt, _ := kong.Request.GetHeader("Authorization")
	dpop, _ := kong.Request.GetHeader("dpop")
	resource, _ := kong.Request.GetHeader("resource")
	url := conf.IAAProxy + resource
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}
	req.Header.Set("Authorization", jwt)
	req.Header.Set("dpop", dpop)
	req.Header.Set("Accept", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Print(err)
	}
	defer resp.Body.Close()
	text_resp, _ := io.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		log.Printf("Error user not authorized. Response status: %d", resp.StatusCode)
		log.Printf("Message: %s", string(text_resp))
		message := []byte(string(text_resp))
		x := make(map[string][]string)
		x["Content-Type"] = append(x["Content-Type"], "application/json")
		kong.Response.Exit(resp.StatusCode,  message, x)
	}
	kong.Response.SetHeader("SSI", "authorized")
}
