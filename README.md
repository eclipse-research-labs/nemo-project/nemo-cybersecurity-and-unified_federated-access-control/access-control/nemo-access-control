# NEMO Access Control

## Description

The Access Control module handles the access to the resources of the NEMO project. It is responsible for the protection of services and the authentication and authorization of users. It employs the Kong Gateway API, an open-source project that enables the use of a variety of security methods like oauth and jwt tokens. An important feature is the ability to create custom plugins to fit the needs of any application. The available custom plugins are the following:

- Keycloak: A plugin that enables Keycloak authentication for both public and private clients.

- SSI: A plugin that enables authentication bases on the Privacy Preserving Self Sovereign Identities component.

The module also uses the [Kong Manager GUI](https://docs.konghq.com/gateway/latest/kong-manager/), an interface for Kong that allows quick and easy management of the Kong services and plugins.

## Prerequisites

Before installing the project please make sure to have:

- Docker installed
- Kubernetes installed and configured
- Helm installed

## Installation

Before proceeding with the installation, make sure to configure the `config/access-control.json` file with the appropriate values for the following parameters/

| Parameters | Description | Example Value |
|     ---    |     ---     |      ---      |
| `namespace` | The namespace to deploy Access Control | `nemo` |
| `password`  |  The password for the PostgresDb | `password` |
| `admin_node_port` | The port where the Kong Admin listens on (necessary to be open Kong Admin as NodePort in order for the Kong Manager to work properly) | `31700` |
| `admin_gui_api_url` | The URL via which the Kong Admin is accessible (necessary to be set in order for the Kong Manager to work properly) | `http://localhost:31700` |

To install the module, run:

```bash
chmod +x install.sh
./install.sh
```

## Usage

After installation the needed ports and services will be installed in the namespace specified. The Kong Manager is the GUI available to interact with Kong. 

 First run:
```bash
kubectl get svc -n {your-namespace}
```
And then from the results check PORT from `nemo-kong-kong-manager`. This will be the port you can visit.

Alternatively, you can run the following commands that produce the link that should be followed to visit the Kong Manager.
```bash
  export NODE_PORT=$(kubectl get --namespace nemo -o jsonpath="{.spec.ports[0].nodePort}" services nemo-kong-kong-manager)
  export NODE_IP=$(kubectl get nodes --namespace nemo-access-control -o jsonpath="{.items[0].status.addresses[0].address}")
  echo http://$NODE_IP:$NODE_PORT
```

Please note that in order to use the Keycloak and SSI plugins you have to have deployed your own instances of them and make the appropriate configurations when setting up the plugins.

## Uninstall

To remove the access control execute the ```uninstall.sh``` script:

```bash
chmod +x uninstall.sh
./uninstall.sh
```

