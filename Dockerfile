
FROM golang:1.18.1 as builder

RUN mkdir /keycloak
COPY /plugins/keycloak.go /keycloak/
RUN cd /keycloak/ && \
  go mod init kong-keycloak && \
  go get github.com/Kong/go-pdk && \
  go get github.com/Kong/go-pdk/server && \
  go build github.com/Kong/go-pdk/server && \
  go get github.com/Nerzal/gocloak/v12 && \
  go build keycloak.go


COPY /plugins/ssi.go /ssi/
RUN cd /ssi/ && \
  go mod init kong-ssi && \
  go get github.com/Kong/go-pdk && \
  go get github.com/Kong/go-pdk/server && \
  go build github.com/Kong/go-pdk/server && \
  go build ssi.go

FROM kong:3.5.0

COPY --from=builder /keycloak/keycloak /tmp/go-plugins
COPY --from=builder /ssi/ssi /tmp/go-plugins

USER root
COPY --from=builder /keycloak/keycloak /usr/local/bin/
COPY --from=builder /keycloak/keycloak /usr/local/share/lua/5.1/kong/plugins/
COPY --from=builder /ssi/ssi /usr/local/bin
COPY --from=builder /ssi/ssi /usr/local/share/lua/5.1/kong/plugins/

USER kong
ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE 8000 8443 8001 8444
STOPSIGNAL SIGQUIT
HEALTHCHECK --interval=10s --timeout=10s --retries=10 CMD kong health
CMD ["kong", "docker-start"]
